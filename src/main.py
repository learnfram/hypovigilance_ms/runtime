#!/usr/bin/env python3
import logging
import pickle
from io import BytesIO

import pandas as pd
from learnfram.application.Application import Application
from learnfram.services.RuntimeComponent import RunTimeComponent


class Service(RunTimeComponent):

    component_type = "runtime"

    def run(self):
        logging.info(f"Starting ...")

        loaded_model = pickle.load(BytesIO(self.model))

        test = pd.DataFrame.from_dict(self.data["test"]["data"])
        annotation = pd.DataFrame.from_dict(self.data["test"]["annotation"])

        result = loaded_model.score(test, annotation)
        # return list de result
        self.send(result)


if __name__ == "__main__":
    Application.start(Service)
